#! /usr/bin/python3

"""
K-mean
 
Usage:
  kmean.py fixe --nbcluster <nb> [--file <file>] --type <type> 
  kmean.py dyn --file <file> --nbcluster-max <nb-max>
  kmean.py -h
  kmean.py --version
   
Examples:

Arguments:

Options:
  -h, --help
  -t <type>, --type <type>              random or file data 'r' : random, 'f' : file
  -f <file>, --file <file>              csv data format
  -n <nb>,   --nbcluster <nb>           number element when the type is random
  -m <nb-max>,   --nbcluster-max <nb-max>
  --version

"""
import os
import sys
import csv

from Matrix import Matrix
from Kmean import Kmean
from Statistiques import Statistiques
from docopt import docopt
from Affichage import *

def main():
    opt             = docopt(__doc__, sys.argv[1:])
    data_file       = opt["--file"]
    data_type       = opt["--type"]
    nb_cluster      = int(opt["<nb>"]) if opt["--nbcluster"] == True else "" 
    nb_cluster_max  = int(opt["<nb-max>"]) if opt["--nbcluster-max"] == True else "" 
    r               = 0
    data            = {}

    if data_file != "":
        with open(data_file, 'r') as csvfile:
            csv_reader = csv.reader(csvfile, delimiter=',', quoting=csv.QUOTE_NONNUMERIC)
            for row in csv_reader:
                data[r] = row
                r += 1

    new_path = "graphs"
    if not os.path.exists(new_path):
        os.makedirs(new_path)

    # Initialisation du k-mean
    kmean       = Kmean(Matrix(data))
    # Création de la matrice de normalisation des distances
    kmean.set_matrice_normalise_distance(Matrix([[],[]]))
    
    # plt.scatter(data[0], data[1], c = "violet", linewidths = 0, edgecolor = "none")
    # plt.axis([-15, 10, -15, 10])
    # plt.savefig("graphs/kmaen_random_0_0.png")
    
    """ 
    Nombre de cluster fixe
    """
    if opt["fixe"] == True:
        # centroid    = kmean.get_ramdom_centroid_in_data(nb_cluster)
        centroid    = Matrix([[0, 4], [5, 0]])
        kmean.set_centroid(centroid)

        # Création de tous les clusters
        kmean.get_all_cluster(centroid, "fixe")
        plot_error_rate(kmean.list_error, "error_rate")

    """
    Nombre de cluster variables
    """
    if opt["dyn"] == True:
        final_graph = kmean.get_best_cluster(nb_cluster_max, "dyn")
        print(final_graph)


if __name__ == "__main__":
    main()
