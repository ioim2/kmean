class Matrix:

    def __init__(self, mat):
        """ Initialisation de la classe Matrix"""
        self.mat        = mat
        self.size_mat   = self.size()
        self.rows       = self.size_mat[0]
        self.cols       = self.size_mat[1]

    def __get__(self, obj, val):
        return self.mat

    def __mul__(self, element):

        if isinstance(element, float) or isinstance(element, int):

            new_mat    = [([0] * self.cols) for i in range(self.rows)]
            for i in range(self.rows):
                for j in range(self.cols):
                    new_mat[i][j] += self.mat[i][j] * element

        else:
            if self.cols != element.rows:
                print("Les matrices ne sont multiables")
            
            element.cols = element.size()[1]
            new_mat    = [([0] * element.cols) for i in range(self.rows)]

            for i in range(self.rows):
                for col in range(element.cols):
                    for j in range(self.cols):
                        new_mat[i][col] += self.mat[i][j] * element.mat[j][col]
                        
        return Matrix(new_mat)

    def __sub__(self, element):
        new_mat    = [([0] * self.cols) for i in range(self.rows)]
        
        if isinstance(element, Matrix):

            for i in range(self.rows):
                for j in range(self.cols):
                    new_mat[i][j] = self.mat[i][j] - element.mat[i][j]

        return Matrix(new_mat)


    def size(self, mat = None):
        if mat is not None:
            return len(mat), len(mat[0])
        else:
            return len(self.mat), len(self.mat[0])
    
    def transpose(self):

        new_mat    = [([0] * self.rows) for i in range(self.cols)]
        for i in range(self.rows):
            for j in range(self.cols):
                new_mat[j][i] = self.mat[i][j]

        return Matrix(new_mat)