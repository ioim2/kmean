from Statistiques import Statistiques
from Matrix import Matrix
from Affichage import *
from random import randrange

class Kmean():

    def __init__(self, data, centroid=Matrix([[],[]])):
        """ Initialisation """
        self.stats      = Statistiques()
        self.data       = data
        self.centroid   = centroid
        self.nb_cluster = self.centroid.cols
        self.clusters   = []
        self.list_error = []

    def set_centroid(self, centroid):
        """ Ajout du centroid """
        self.centroid = centroid
        self.nb_cluster = len(self.centroid.mat[0])

    def get_centroid(self):
        return self.centroid
    
    def calcul_centroid(self):
        """ Récupération des centroïde """
        self.centroid.mat = [[],[]]
        for cluster in self.clusters:
            self.centroid.mat[0].append(self.stats.moyenne(cluster[0]))
            self.centroid.mat[1].append(self.stats.moyenne(cluster[1]))
        pass

    def get_cluster(self):
        """ Récupération des clusters """
        self.clusters = [[[] for h in range(2)] for j in range(self.nb_cluster)]

        for i in range(self.data.cols):

            current_dist = []

            for k in range(self.nb_cluster):
                p1 = Matrix([[self.centroid.mat[0][k]], [self.centroid.mat[1][k]]])
                p2 = Matrix([[self.data.mat[0][i]], [self.data.mat[1][i]]])

                current_dist.append(self.distance_normalise(p1, p2))

            pos = self.stats.min(current_dist)
            
            self.clusters[pos[0]][0].append(self.data.mat[0][i])
            self.clusters[pos[0]][1].append(self.data.mat[1][i])

        pass

    def get_all_cluster(self, centroid, file_name="command", set_picture=True):
        """ Détermine les centres de chaque cluster """
        for i in range(15):
            if i == 0:
                self.set_centroid(centroid)
            else:
                self.set_centroid_by_cluster()

            self.get_cluster()

            # print(self.get_centroid().mat)

            if set_picture == True:
                plot_scatter_n_classe(self.centroid.mat, self.clusters, "graphs/kmean_"+file_name+"_"+str(self.centroid.cols)+"_"+str(i))

            # Définition du taux d'erreur
            self.set_error_rate()
            
            if i > 0 and (self.list_error[i-1] - self.list_error[i]) == 0:
                break
    
    def get_best_cluster(self, nb_cluster, file_name="command", save_picture=True):
        list_part_centroid  = [[],[]]
        final_graph         = []

        centroid            = self.get_ramdom_centroid_in_data(nb_cluster)

        for i in range(nb_cluster):
            # Récupération des centroides
            list_part_centroid[0] = centroid.mat[0][:i+1]
            list_part_centroid[1] = centroid.mat[1][:i+1]
            # print(list_part_centroid)

            self.set_centroid(Matrix(list_part_centroid))
            self.get_all_cluster(Matrix(list_part_centroid), file_name, save_picture)

            # print(kmean.get_error_list())
            final_graph.append(self.stats.min(self.list_error)[1])
            self.list_error = []

        return final_graph


    def distance_normalise(self, p1, p2):
        """ Normalisation des distances """
        x1      = p1.mat[0][0] - p2.mat[0][0]
        x2      = p1.mat[1][0] - p2.mat[1][0]
        x       = Matrix([[x1], [x2]])
        return (x.transpose() * self.mat_norm * x).mat

    def set_matrice_normalise_distance(self, mat_norm):
        """ Création de la matrice pour normaliser les distances """
        mat_norm.mat = [
            [1/self.stats.variance(self.data.mat[0]),0], 
            [0,1/self.stats.variance(self.data.mat[1])]
        ]
        self.mat_norm = mat_norm

        pass
        
    def set_centroid_by_cluster(self):
        """ Récupération des centroïde """
        g = [[],[]]
        for cluster in self.clusters:     
            g[0].append(self.stats.moyenne(cluster[0]))
            g[1].append(self.stats.moyenne(cluster[1]))
        
        self.centroid = Matrix(g)
        
        pass

    def get_ramdom_centroid_in_data(self, nb_cluster):
        """ Choix aléatoire des index du tableau """
        list_index      = []
        list_centroid   = Matrix([[],[]])

        # Choix aléatoire des index du tableau
        for i in range(nb_cluster):
            index       = randrange(len(self.data.mat[0]))
            
            while index in list_index:
                index       = randrange(len(self.data.mat[0]))
            list_index.append(index)

        # Ajout des centroids
        for x in list_index:
            list_centroid.mat[0].append(self.data.mat[0][x])
            list_centroid.mat[1].append(self.data.mat[1][x])

        return list_centroid

    def set_error_rate(self):
        """ Calcul des distances de chaque cluster """
        somme_dist      = 0
        for pos, cluster in enumerate(self.clusters):
            for i, val in enumerate(cluster[0]):
                p1 = Matrix([[val],[self.clusters[pos][1][i]]])
                p2 = Matrix([[self.centroid.mat[0][pos]],[self.centroid.mat[1][pos]]])
                somme_dist += self.distance_normalise(p1, p2)[0][0]

        self.list_error.append(somme_dist)

    def get_error_list(self):
        return self.list_error