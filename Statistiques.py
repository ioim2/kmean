from Matrix import Matrix

class Statistiques:

    def __init__(self):
        """ Initialisation """

    def min(self, vecteur):
        val     = None
        index   = 0
        """ Trouver le plus petit élément d'un vecteur """
        for i, test_val in enumerate(vecteur):
            if val is None:
                val     = test_val
                index   = i
            elif test_val < val:
                val     = test_val
                index   = i

        return [index, val]

    def max(self, vecteur):
        val = None
        """ Trouver le plus grand élément d'un vecteur """
        for test_val in vecteur:
            if val is None:
                val = test_val
            if test_val > val:
                val = test_val

        return val

    def carre(self, value):
        """ Calcul le carré d'une valeur """

        if isinstance(value, Matrix):
            return value * value.transpose()
        else:
            return value * value

    def moyenne(self, vecteur):
        """ Calcul de la moyenne d'un vecteur """
        val = 0
        for sum_val in vecteur:
            val += sum_val
            
        val = val / len(vecteur)

        return val

    def mediane(self, vecteur):
        """ Calcul de la médiane """
        c_vecteur = vecteur.copy()
        for key, val in enumerate(vecteur):
            c_inf = 0
            c_sup = 0
            for c_key, c_val in enumerate(c_vecteur):
                if key != c_key:
                    if val > c_val:
                        c_inf += 1
                    elif val <= c_val:
                        c_sup += 1

            if c_inf == c_sup or c_inf + 1 == c_sup or c_inf - 1 == c_sup:
                return val

        return None

    def racine_carre(self, value):
        """ Calcul de la racine carré """
        c_val           = value
        intermediaire   = 0
        index           = 0

        # for i in range(1, 10):
        while c_val != intermediaire:
            intermediaire = c_val
            c_val = (0.5 * c_val) + (0.5 * (value / c_val))
            if index > 1000:
                break
            index += 1

        return c_val

    def variance(self, vecteur):
        """ Calcul de l'écart type """
        c_val       = 0
        len_vecteur = len(vecteur)
        moyenne     = self.moyenne(vecteur)

        for val in vecteur:
            c_val += (val - moyenne) * (val - moyenne)

        return c_val / len_vecteur

    def covariance(self, vecteur_1, vecteur_2):
        """ Calcul de la covariance """
        c_val       = 0
        len_vect_1  = len(vecteur_1)
        len_vect_2  = len(vecteur_2)
        moyenne_1   = self.moyenne(vecteur_1)
        moyenne_2   = self.moyenne(vecteur_2)

        if len_vect_1 != len_vect_2:
            exit

        for key in range(len(vecteur_1)):
            c_val += (vecteur_1[key] - moyenne_1) * (vecteur_2[key] - moyenne_2)

        return c_val / len_vect_1

    def corelation(self, vecteur_1, vecteur_2):
        """ Calcul de la corélation entre deux vecteur """
        covar   = self.covariance(vecteur_1, vecteur_2)
        ect_1   = self.racine_carre(self.variance(vecteur_1))
        ect_2   = self.racine_carre(self.variance(vecteur_2))

        return covar / (ect_1 * ect_2)

    def matrice_corelation(self, matrice):
        """ Calcul de la matric de corelation """
        l       = len(matrice)
        mat_cor = [([0] * l) for i in l]
        # [[0 for x in range(l)] for y in range(l)]
    
        for key_1 in range(len(matrice)):
            for key_2 in range(len(matrice)):
                if key_1 <= key_2:
                    mat_cor[key_1][key_2] = self.corelation(matrice[key_1], matrice[key_2])
        
        return mat_cor

    def scalaire(self, mat):
        """ Convetion d'une matrice 1x1 en scalaire """
        if mat.rows == 1 and mat.cols == 1:
            return mat.mat[0][0]

        return mat

    def distance_euclidienne(self, p1, p2):
        """ Calcul de la distance euclidienne """
        x1      = self.carre(p1[0] - p2[0])
        x2      = self.carre(p1[1] - p2[1])

        return self.racine_carre(x1 + x2)

    # def distance_normalisee(self, p1, p2, M):
    #     """ Distance normalisée """
    #     dist    = Matrix([[p1[0] - p2[0], p1[1] - p2[1]]])

    #     return dist  

    # def centroide(self, clusters):
    #     """ Récupération des centroïde """
    #     g = [[],[]]
    #     for cluster in clusters:

    #         g[0].append(self.moyenne(cluster[0]))
    #         g[1].append(self.moyenne(cluster[1]))
    #     return g

    # def coalescence(self, x, K, M, g):
    #     """ Définition des clusters """
    #     clusters = [[[] for h in range(2)] for j in range(K)]
        
    #     for i in range(len(x[0])):

    #         current_dist = []

    #         for k in range(K):
    #             p1 = [g[0][k], g[1][k]]
    #             p2 = [x[0][i], x[1][i]]
                
    #             current_dist.append(self.distance_normalisee(p1, p2, M))
            
    #         pos = self.min(current_dist)

    #         clusters[pos[0]][0].append(x[0][i])
    #         clusters[pos[0]][1].append(x[1][i])

    #     return [clusters, g]

    # def transforme_cluster(self, cluster):
    #     """ Conversion du cluster """
    #     data        = {}
    #     data[0]     = []
    #     data[1]     = []
    #     for i, classe in enumerate(cluster):
    #         for j, ele in enumerate(classe):
    #             data[j] += ele

    #     return data

    # def matrix_norm(self, data):
    #     """ Matrice de normalisation """
    #     variance = [0,0]
    #     for i in range(len(data)):
    #         variance[i] = self.variance(data[i])

    #     return Matrix([[variance[0], 0], [0, variance[1]]])
    #         # for j in range(len(data[i])):
    #         #     data[i][j] /= variance
                
       