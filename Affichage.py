import matplotlib.pyplot as plt
from Statistiques import Statistiques

def plot_scatter_n_classe(centroid, data, filename):
    axis    = [0,0,0,0]
    stats   = Statistiques()
    color   = ["red", "lightgray", "chocolate", "violet", "yellow", "green", "blue", "orange", "pink", "khaki"]

    for i in range(len(centroid[0])):
        plt.scatter(data[i][0], data[i][1], c = color[i], linewidths = 0, edgecolor = "none")
        x1 = stats.min(data[i][0])[1]
        x2 = stats.max(data[i][0])
        y1 = stats.min(data[i][1])[1]
        y2 = stats.max(data[i][1])

        if x1 < axis[0]:     axis[0] = x1
        if x2 > axis[1]:     axis[1] = x2
        if y1 < axis[2]:     axis[2] = y1
        if y2 > axis[3]:     axis[3] = y2
    
    plt.scatter(centroid[0], centroid[1], c = 'black', marker = "s")
    plt.axis([axis[0] + axis[0] * 0.1, axis[1] + axis[1] * 0.1, axis[2] + axis[2] * 0.1, axis[3] + axis[3] * 0.1])
    plt.savefig(filename)
    plt.close()
    # plt.show()

def plot_error_rate(error_rate, filename):
    plt.plot(error_rate)
    plt.savefig("graphs/kmean_"+filename)